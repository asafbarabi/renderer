from datetime import timedelta
from composition import Composition
from layer import Layer
from abstarct_engine import AbstractEngine


class Renderer:
    def __init__(self, video_engine: AbstractEngine):
        self.video_engine = video_engine

    def render(self, compos: Composition):
        current_index = 0
        compos.layers.sort(key=lambda l: l.start_time)
        frame = 0
        current_layers = []
        temp_list_layers = []
        while frame <= (compos.fps * compos.duration):
            just_started_layers, current_index = self.take_while(compos.layers, frame, current_index, compos.fps)
            current_layers = current_layers + just_started_layers
            current_layers.sort(key=lambda l: l.z)
            updated_layer = Layer("", self.video_engine.create_base_image_array(compos.height, compos.width))
            for layer in current_layers:
                updated_layer = self.put_layer_on_layer(layer, updated_layer)
                layer.duration -= timedelta(seconds=1 / compos.fps)
                if layer.duration.total_seconds() > 0:
                    temp_list_layers.append(layer)
            self.showLayer(updated_layer)
            self.video_engine.add_frame(updated_layer.img_array)
            frame += 1
            current_layers = temp_list_layers
            temp_list_layers = []
        self.video_engine.release_all()

    def take_while(self, layers, frame, from_index, fps):
        res_list = []
        if from_index > len(layers) - 1:
            return res_list, from_index

        time = timedelta(seconds=(1 / fps) * frame)
        for layer in layers[from_index:]:
            if layer.start_time <= time:
                from_index = from_index + 1
                res_list.append(layer)
            else:
                return res_list, from_index
        return res_list, from_index

    def resize_layer(self, layer):
        resized_array = self.video_engine.resize_image(layer.img_array, layer.width, layer.height)
        layer.img_array = resized_array
        layer.scale = 1
        layer.changed = False
        return layer.img_array

    def showLayer(self, layer):
        window_name = 'layer'
        if layer.image != "":
            self.video_engine.show_image(self.video_engine.read_image(layer.image))
        else:
            self.video_engine.show_image(layer.img_array)

    def crop_layer(self, layer: Layer, new_width, new_height):
        img = self.video_engine.read_image(layer.image)
        if new_width == layer.width and new_height == layer.height:
            return img

        return self.video_engine.crop_image(img, new_width, new_height)

    def put_image_on_image(self, src, dst, x_offset, y_offset):
        src_new_width, src_new_height = src.shape[1], src.shape[0]
        if dst.shape[1] < x_offset + src_new_width:
            src_new_width = dst.shape[1] - x_offset
        if dst.shape[0] < y_offset + src_new_height:
            src_new_height = dst.shape[0] - y_offset

        cropped_layer = self.video_engine.crop_image(src, src_new_width, src_new_height)
        self.video_engine.put_image_on_image(cropped_layer, dst, x_offset, y_offset)
        return dst

    def put_layer_on_layer(self, src, dst):
        src_array = self.get_layer_image_array(src)
        dst_array = self.get_layer_image_array(dst)
        new_array = self.put_image_on_image(src_array, dst_array, src.position[0], src.position[1])
        return Layer("", new_array)

    def get_layer_image_array(self, layer):
        if layer.img_array != []:
            if not layer.changed:
                return layer.img_array
            else:
                return self.resize_layer(layer)
        else:
            self.video_engine.read_image(layer.image)

