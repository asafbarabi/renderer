import cv2
from datetime import timedelta


class Layer:
    def __init__(self, image_file_name="", image_array=[], position=(0, 0), z=1, scale=1, start_time=0, duration=1,
                 opacity=1):
        self.image = str(image_file_name)
        self.position = position
        if image_file_name != "":
            self.img_array = cv2.imread(str(image_file_name))
        else:
            self.img_array = image_array

        self.height = self.img_array.shape[0]
        self.width = self.img_array.shape[1]
        self.z = z
        self.scale = scale
        self.changed = False
        self.start_time = start_time
        self.duration = timedelta(seconds=duration)
        self.opacity = opacity

    def resize(self, new_width, new_height):
        self.height = new_width
        self.width = new_height
        self.changed = True

    def rescale(self, scaling_factor):
        self.scale = scaling_factor
        self.height = scaling_factor * self.height
        self.width = scaling_factor * self.width
        self.changed = True

    def update_img_array(self, image_array):
        self.img_array = image_array
        self.width = image_array.shape[1]
        self.height = image_array.shape[0]
        self.scale = 1
