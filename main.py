from datetime import timedelta
from pathlib import Path
from composition import Composition
from layer import Layer
from engine import Engine
from renderer import Renderer

file_name = str(Path("C:/Users/USER001/Documents/creatives/somePictures/a.png"))
file_name2 = str(Path("C:/Users/USER001/Documents/creatives/somePictures/b.png"))
file_name3 = str(Path("C:/Users/USER001/Documents/creatives/somePictures/C.png"))

com = Composition(720, 1280, [], 2, 5)
lay = Layer(file_name, position=(300, 200), z=5, start_time=timedelta(seconds=1))
lay3 = Layer(file_name3, position=(0, 0), z=8, start_time=timedelta(0))
lay2 = Layer(file_name2, position=(700, 100), z=3, start_time=timedelta(0))
com.add_layer(lay)
com.add_layer(lay2)
com.add_layer(lay3)
video_name_path = str(Path("C:/Users/USER001/Documents/creatives/somePictures/video12.mp4"))
some_engine = Engine(com.fps, video_name_path, com.width, com.height)
some_renderer = Renderer(some_engine)
some_renderer.render(com)
