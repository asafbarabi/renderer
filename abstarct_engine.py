from abc import ABC, abstractmethod


class AbstractEngine(ABC):

    @abstractmethod
    def read_image(self, image_path):
        pass

    @abstractmethod
    def create_base_image_array(self, height, width, color="black"):
        pass

    @abstractmethod
    def resize_image(self, img_array, new_width, new_height):
        pass

    @abstractmethod
    def crop_image(self, img_array, new_width, new_height):
        pass

    @abstractmethod
    def put_image_on_image(self, src, dst, x_offset, y_offset):
        pass

    @abstractmethod
    def set_fps(self, fps):
        pass

    @abstractmethod
    def set_video_save_path(self, video_path):
        pass

    @abstractmethod
    def add_frame(self, img_array):
        pass

    @abstractmethod
    def release_all(self):
        pass

    def show_image(self, img_array):
        pass
