from abstarct_engine import AbstractEngine
import cv2
import numpy as np


class Engine(AbstractEngine):
    def __init__(self, fps, video_path, width, height):
        self.fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        self.fps = fps
        self.video_path = video_path
        self.video_writer = cv2.VideoWriter(video_path, self.fourcc, fps, (width, height))

    def read_image(self, image_path):
        return cv2.imread(image_path)

    def set_fps(self, fps):
        self.fps = fps

    def set_video_save_path(self, video_path):
        self.video_path = video_path

    def create_base_image_array(self, height, width, color="black"):
        if color == "black":
            frame = 0 * np.ones((height, width, 3), np.uint8)
        else:
            frame = 255 * np.ones((height, width, 3), np.uint8)
        return frame

    def resize_image(self, img_array, new_width, new_height):
        return cv2.resize(img_array, (new_width, new_height), interpolation=cv2.INTER_AREA)

    def crop_image(self, img_array, new_width, new_height):
        return img_array[0:new_height, 0:new_width]

    def put_image_on_image(self, src, dst, x_offset, y_offset):
        dst[y_offset:y_offset + src.shape[0], x_offset:x_offset + src.shape[1]] = src

    def add_frame(self, img_array):
        self.video_writer.write(img_array)

    def show_image(self, img_array):
        cv2.imshow("some name", img_array)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def release_all(self):
        cv2.destroyAllWindows()
        self.video_writer.release()
