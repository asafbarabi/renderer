from datetime import timedelta


class Composition:
    def __init__(self, height, width, layers=[], fps=1, duration=1):
        self.layers = layers
        self.height = height
        self.width = width
        self.fps = fps
        self.duration = duration

    def add_layer(self, layer):
        self.layers.append(layer)

    def remove_layer(self, layer):
        self.layers.remove(layer)

    def change_resolution(self, height, width):
        self.height = height
        self.width = width

    def change_fps(self, fps):
        self.fps = fps

    def change_duration(self, duration):
        self.duration = duration
